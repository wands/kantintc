<?php
if(!isset($_SESSION)) 
{ 
	session_save_path();
	session_start(); 
}  
if(!isset($_COOKIE['user']) || $_SESSION['user']!=session_id() || $_SESSION['user']==NULL)
{
	 echo "<script>window.location.replace(\"login\");</script>";
 	//header("location:Login.php");
}?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pratt - Free Bootstrap 3 Theme">
    <meta name="author" content="Alvarez.is - BlackTie.co">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">

    <title>Sistem Informasi Kantin Teknik Informatika ITS</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
    

  </head>

  <body data-spy="scroll" data-offset="0" data-target="#navigation">

    <!-- Fixed navbar -->
	    <div id="navigation" class="navbar navbar-default navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#"><b>Selamat Datang, Admin</b></a>
	        </div>
	        <div class="navbar-collapse collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="<?php echo site_url('Welcome/home')?>" class="smothscroll">Halaman Muka</a></li>
				<li><a href="#" class="smothScroll">></a></li>
				<li><a href="<?php echo site_url('Welcome/administrasi')?>" class="smothscroll">Administrasi</a></li>
				<li><a href="#" class="smothScroll">></a></li>
				<li class="active"><a href="#">Laporan Keuangan</a></li>
				<li><a href="<?php echo site_url('Welcome/tambahbarang') ?>" class="smothScroll">Tambah Barang</a></li>
				<li><a href="#" class="smothScroll"></a></li>
				<li><a href="<?php echo site_url('Welcome/dummylogout')?>" class="smothScroll">Logout</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>
		
	<section id="desc" name="desc"></section>
	<!-- INTRO WRAP -->
		<!-- INTRO WRAP -->
	<div id="intro">
		<div class="container">
		<div align="center">
		<img src="<?php echo base_url(); ?>img/laporan.png" alt="" style="width:75px; height:100px; display:inline-block">  
			<table>
				<tr>
				
				</tr>
				<tr>
					<td>
						<div style="width:20px"></div>
					</td>
					<td>
						<h3 style="text-align:left display:inline-block;"><strong>Laporan Keuangan Kantin Teknik Informatika ITS</strong></h3>
					</td>
				</td>
			</table>
		</div>
		<hr>
		
		<div align="center">

			<strong>Laporan Keuangan Bulan <?php 
			if ($_POST['bulan'] == 1) echo 'Januari';
			if ($_POST['bulan'] == 2) echo 'Februari';
			if ($_POST['bulan'] == 3) echo 'Maret';
			if ($_POST['bulan'] == 4) echo 'April';
			if ($_POST['bulan'] == 5) echo 'Mei';
			if ($_POST['bulan'] == 6) echo 'Juni';
			if ($_POST['bulan'] == 7) echo 'Juli';
			if ($_POST['bulan'] == 8) echo 'Agustus';
			if ($_POST['bulan'] == 9) echo 'September';
			if ($_POST['bulan'] == 10) echo 'Oktober';
			if ($_POST['bulan'] == 11) echo 'November';
			if ($_POST['bulan'] == 12) echo 'Desember';
			?></strong>
						
			<?php $angka=$_POST['bulan']; 
			$query=$this->db->query("call sp_laporan_keuntungan($angka)");
			foreach ($query->result() as $row) {?>
				<table align="center">
				<tr>
					<td style="width:300px"><p style="font-size:1.25em">Pemasukan total</p></td>
					<td style='width:100px'><p style='font-size:1em'><?php
						echo $row->Penjualan;
					?></p></td>
					</td>
				</tr>
				<tr>
					<td style="width:300px"><p style="font-size:1.25em">Pengeluaran total</p></td>
					<td style='width:100px'><p style='font-size:1em'><?php
						echo $row->Pembelian;
					?></p></td>
				</tr>
				<tr>
					<td colspan="2" style="width:300px; height:1px; background-color:#2F2F2F"></td>
				</tr>
				<tr>
					<td style="width:300px"><p style="font-size:1.25em">Keuntungan</p></td>
					<td style='width:100px'><p style='font-size:1em'><?php
						echo $row->Keuntungan; 
						}
					?></p></td>
				</tr>
				</table>		
			<br><br>
			<div align="center">
				<a href="<?php echo site_url('Welcome/listbarang')?>"><button class="btn btn-large btn-success">LIHAT STOK</button></a>
				&nbsp; &nbsp; &nbsp;
				<a href="<?php echo site_url('Welcome/laporan2')?>"><button class="btn btn-large btn-success">KEMBALI</button></a>
			</div>
		</div>
		
		<hr>
	    </div> <!--/ .container -->
	</div><!--/ #introwrap -->
		<section id="contact" name="contact"></section>
	<div id="footerwrap">
		<div class="container">
			<div class="col-lg-5">
				<h3>Kantin Teknik Informatika ITS</h3>
				<p>
				Jl. Teknik Kimia, Sukolilo, Surabaya <br>
				Telepon: 0812345678 &nbsp; | &nbsp; Facebook: Kantin TC <br> Twitter: @kantinTC &nbsp; | &nbsp; Instagram: @kantinTC
				</p>
			</div>
			
			<div class="col-lg-7">
				<p style="float:right; margin-top:10%">Created by &nbsp;<img src="<?php echo base_url(); ?>img/Logov3.png" style="max-width:125px"></img></p>	
			</div>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script>
	$('.carousel').carousel({
	  interval: 3500
	})
	</script>
  </body>
</html>