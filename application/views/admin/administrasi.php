<?php
if(!isset($_SESSION)) 
{ 
	session_save_path();
	session_start(); 
}  
if(!isset($_COOKIE['user']) || $_SESSION['user']!=session_id() || $_SESSION['user']==NULL)
{
	 echo "<script>window.location.replace(\"login\");</script>";
 	//header("location:Login.php");
}?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pratt - Free Bootstrap 3 Theme">
    <meta name="author" content="Alvarez.is - BlackTie.co">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">

    <title>Sistem Informasi Kantin Teknik Informatika ITS</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
    

  </head>

  <body data-spy="scroll" data-offset="0" data-target="#navigation">

    <!-- Fixed navbar -->
	    <div id="navigation" class="navbar navbar-default navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#"><b>Selamat Datang, Admin</b></a>
	        </div>
	        <div class="navbar-collapse collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="<?php echo site_url('Welcome/home')?>" class="smothscroll">Halaman Muka</a></li>
				<li><a href="#" class="smothScroll">></a></li>
	            <li class="active"><a href="<?php echo site_url('Welcome/administrasi') ?>">Administrasi</a></li>
	            <li><a href="<?php echo site_url('Welcome/barangadmin')?>" class="smothScroll">Stok Barang</a></li>
				<li><a href="<?php echo site_url('Welcome/bukatutup')?>" class="smothScroll">Buka/Tutup Kantin</a></li>
	            <li><a href="<?php echo site_url('Welcome/transaksi')?>" class="smothScroll">Transaksi</a></li>
				<li><a href="#" class="smothScroll"></a></li>
				<li><a href="<?php echo site_url('Welcome/dummylogout')?>" class="smothScroll">Logout</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>
		
	<section id="desc" name="desc"></section>
	<!-- INTRO WRAP -->
	<div id="intro">
		<div class="container">
		<div align="center">
		<img src="<?php echo base_url(); ?>img/Clipboard.png" alt="" style="width:50px; display:inline-block">  
			<table>
				<tr>
				
				</tr>
				<tr>
					<td>
						<div style="width:20px"></div>
					</td>
					<td>
						<h3 style="text-align:left display:inline-block;"><strong>Administrasi Kantin Teknik Informatika ITS</strong></h3>
					</td>
				</td>
			</table>
		</div>
		<hr>
		
		<div style="height:5%"></div>
		
			<div class="row centered">				
				<div class="col-lg-6">
					<img src="<?php echo base_url(); ?>img/laporan.png" alt="" style="height:125px">
					<h3>Laporan</h3>
					<br><a href="<?php echo site_url('Welcome/laporan2') ?>"><button class="btn btn-large btn-success">MASUK</button></a>
				</div>
				<div class="col-lg-6">
					<img src="<?php echo base_url(); ?>img/Pensils.png" alt="" style="height:125px">
					<h3>Tambah Barang</h3>
					<br><a href="<?php echo site_url('Welcome/tambahbarang') ?>"><button class="btn btn-large btn-success">MASUK</button></a>
				</div>
			</div>
			<br>
			<hr>
	    </div> <!--/ .container -->
	</div><!--/ #introwrap -->
		<section id="contact" name="contact"></section>
	<div id="footerwrap">
		<div class="container">
			<div class="col-lg-5">
				<h3>Kantin Teknik Informatika ITS</h3>
				<p>
				Jl. Teknik Kimia, Sukolilo, Surabaya <br>
				Telepon: 0812345678 &nbsp; | &nbsp; Facebook: Kantin TC <br> Twitter: @kantinTC &nbsp; | &nbsp; Instagram: @kantinTC
				</p>
			</div>
			
			<div class="col-lg-7">
				<p style="float:right; margin-top:10%">Created by &nbsp;<img src="<?php echo base_url(); ?>img/Logov3.png" style="max-width:125px"></img></p>	
			</div>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script>
	$('.carousel').carousel({
	  interval: 3500
	})
	</script>
  </body>
</html>