<?php
if(!isset($_SESSION)) 
{ 
	session_save_path();
	session_start(); 
}  
if(!isset($_COOKIE['user']) || $_SESSION['user']!=session_id() || $_SESSION['user']==NULL)
{
	 echo "<script>window.location.replace(\"login\");</script>";
 	//header("location:Login.php");
}?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pratt - Free Bootstrap 3 Theme">
    <meta name="author" content="Alvarez.is - BlackTie.co">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">

    <title>Sistem Informasi Kantin Teknik Informatika ITS</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
    

  </head>

  <body data-spy="scroll" data-offset="0" data-target="#navigation">

    <!-- Fixed navbar -->
	    <div id="navigation" class="navbar navbar-default navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#"><b>Selamat Datang, Admin</b></a>
	        </div>
	        <div class="navbar-collapse collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="<?php echo site_url('Welcome/home')?>" class="smothscroll">Halaman Muka</a></li>
				
				<li><a href="#" class="smothScroll">></a></li>
				<li><a href="<?php echo site_url('Welcome/barangadmin')?>" class="smothscroll">Stok Barang</a></li>
				<li><a href="#" class="smothScroll">></a></li>
				<li class="active"><a href="#" class="smothScroll">Lihat List Barang</a></li>
	            <li><a href="<?php echo site_url('Welcome/tambahstok') ?>">Tambah Stok</a></li>
				<li><a href="#" class="smothScroll"></a></li>
				<li><a href="<?php echo site_url('Welcome/dummylogout')?>" class="smothScroll">Logout</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>

	<section id="desc" name="desc"></section>
	<!-- INTRO WRAP -->
	<div id="intro">
		<div class="container">
		<div align="center">
		<img src="<?php echo base_url(); ?>img/kerdusmaneh.png" alt="" style="width:75px; display:inline-block">  
			<table>
				<tr>
				
				</tr>
				<tr>
					<td>
						<h3 style="text-align:left display:inline-block;"><strong>List Barang</strong></h3>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td>
					<a href="<?php echo site_url('Welcome/listbarang')?>"><button class="btn btn-large btn-success">SEMUA BARANG</button></a>
					</td>
					<td style="width:25px">
					</td>
					<td>
					<a href="<?php echo site_url('Welcome/kadaluarsa')?>"><button class="btn btn-large btn-success">KADALUARSA HARI INI</button></a>
					</td>
					<td style="width:25px">
					</td>
					<td>
					<a href="<?php echo site_url('Welcome/tipis')?>"><button class="btn btn-large btn-success">STOK MENIPIS</button></a>
					</td>
					<td style="width:25px">
					</td>
					<td>
					<a href="<?php echo site_url('Welcome/laris')?>"><button class="btn btn-large btn-success">BERDASAR JUMLAH PENJUALAN</button></a>
					</td>
				</tr>
			</table>
		</div>
		<hr>
		<div>
			<div align="center">
			<strong>List Semua Barang</strong><br><br>
			<table class="table table-striped">
			<thead>
			<tr style="text-align:center">
				<td style="width:250px"><p style="font-size:1.25em">Barang</p></td>
				<td style="width:250px"><p style="font-size:1.25em">Harga Beli</p></td>
				<td style="width:250px"><p style="font-size:1.25em">Harga Jual</p></td>
				<td style="width:250px"><p style="font-size:1.25em">Jumlah</p></td>
			</tr>
			</thead>
			<?php
				foreach ($barangnya->result() as $row) {
			?>
			
			<tr style='text-align:center'>
				<td style='width:250px'><p style='font-size:1em'><?php echo $row->Barang; ?></p></td>
				<td style='width:250px'><p style='font-size:1em'><?php echo $row->Beli; ?></p></td>
				<td style='width:250px'><p style='font-size:1em'><?php echo $row->Harga; ?></p></td>
				<td style='width:250px'><p style='font-size:1em'><?php echo $row->Jumlah; ?></p></td>
			</tr>
			
			<?php
				}
			?>
			</table></div>
			</br>
			</div>	
	    </div> <!--/ .container -->
	    <hr>
	</div><!--/ #introwrap -->
		<section id="contact" name="contact"></section>
	<div id="footerwrap">
		<div class="container">
			<div class="col-lg-5">
				<h3>Kantin Teknik Informatika ITS</h3>
				<p>
				Jl. Teknik Kimia, Sukolilo, Surabaya <br>
				Telepon: 0812345678 &nbsp; | &nbsp; Facebook: Kantin TC <br> Twitter: @kantinTC &nbsp; | &nbsp; Instagram: @kantinTC
				</p>
			</div>
			
			<div class="col-lg-7">
				<p style="float:right; margin-top:10%">Created by &nbsp;<img src="<?php echo base_url(); ?>img/Logov3.png" style="max-width:125px"></img></p>	
			</div>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
	<script>
	$('.carousel').carousel({
	  interval: 3500
	})
	</script>
  </body>
</html>