<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct(){
		parent::__construct();
		$this->load->model('');
		$this->load->database();
		$this->load->model('kantin');
	}
	 
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function tes()
	{
		echo "tes";
		//$this->load->view('coba');
	}
	
	public function kongou()
	{
		$this->load->helper('url');
		$this->load->view('kongou');
	}
	
	public function login()
	{
		$this->load->helper('url');
		$this->load->view('login');
	}
	
	//orang
	public function jadwal()
	{
		$data['abc'] = $this->kantin->bukatutup();
		$this->load->helper('url');
		$this->load->view('orang/jadwal', $data);
	}
	
	public function barang()
	{
		//$data['barangnya'] = $this->kantin->barang();
		$data['barangnya'] = $this->db->query("call sp_daftar_barang()");
		$this->load->helper('url');
		$this->load->view('orang/barang', $data);
	}
	
	//admin
	public function home()
	{
		$data['notif']=$this->db->query("call sp_notifikasi()");
		$this->load->helper('url');
		$this->load->view('admin/home', $data);
	}
	
	public function administrasi()
	{
		$this->load->helper('url');
		$this->load->view('admin/administrasi');
	}
	
	public function laporan()
	{
		//$data['beli'] = $this->db->query("call sp_laporan_pembelian()");
		//$data['jual'] = $this->db->query("call sp_laporan_penjualan()");
		//$data['untung'] = $this->db->query("call sp_laporan_keuntungan()");
		$this->load->helper('url');
		$this->load->view('admin/laporan');
	}
	
	public function laporan2()
	{
                $this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');

              	$this->load->view('admin/laporan2');
	}
	
	public function barangadmin()
	{
		$this->load->helper('url');
		$this->load->view('admin/barangadmin');
	}
	
	public function tambahbarang()
	{
		$this->load->helper('url');
		$this->load->view('admin/tambahbarang');
	}
	
	public function addb()
	{
		$this->load->helper('url');
		$this->load->view('admin/addb');
	}
	
	public function listbarang()
	{
		$data['barangnya'] = $this->db->query("call sp_daftar_barang()");
		$this->load->helper('url');
		$this->load->view('admin/listbarang', $data);
	}
	
	public function tipis()
	{
		$data['barangnya'] = $this->db->query("call sp_stok_tipis()");
		$this->load->helper('url');
		$this->load->view('admin/tipis', $data);
	}
	
	public function tambahstok()
	{
		$data['barangnya'] = $this->db->query("call sp_daftar_barang()");
		$this->load->helper('url');
		$this->load->view('admin/tambahstok', $data);
	}
	
	public function adds()
	{
		$this->load->helper('url');
		$this->load->view('admin/adds');
	}
	
	public function kadaluarsa()
	{
		$data['barangnya'] = $this->db->query("call sp_barang_kadaluarsa()");
		$this->load->helper('url');
		$this->load->view('admin/kadaluarsa', $data);
	}
	
	public function kadal()
	{
		$this->db->query("call sp_hapus_kadaluarsa()");
		$this->load->helper('url');
		$this->load->view('admin/kadal');
	}
	
	public function laris()
	{
		$data['barangnya'] = $this->db->query("call sp_beli_terbanyak()");
		$this->load->helper('url');
		$this->load->view('admin/laris', $data);
	}
	
	public function transaksi()
	{
		//$data['barangnya'] = $this->db->query("call sp_daftar_barang()");
		//$data['trans'] = $this->kantin->trans();
		//$data['id'] = $this->db->query("call sp_id_transaksi()");
		$this->load->helper('url');
		//$this->load->view('admin/transaksi', $data);
		$this->load->view('admin/transaksi');
	}
	
	public function dummytransaksi()
	{
		$this->load->helper('url');
		$this->load->view('admin/dummytransaksi');
	}

	public function dummylogin()
	{
		$this->load->helper('url');
		$this->load->view('admin/dummylogin');
	}

	public function dummylogout()
	{
		$this->load->helper('url');
		$this->load->view('admin/dummylogout');
	}
	
	public function bayar()
	{
		$data['bayarnya'] = $this->db->query("call sp_harga_total()");
		$this->load->helper('url');
		$this->load->view('admin/bayar', $data);
	}
	
	public function bukatutup()
	{
		$data['abc'] = $this->kantin->bukatutup();
		$this->load->helper('url');
		$this->load->view('admin/bukatutup', $data);
	}
	
	public function open()
	{
		$this->db->query("call sp_buka_tutup");
		$this->load->helper('url');
		$this->load->view('admin/open');
	}
	
	public function tampilan2()
	{
		$this->load->view('coba');
	}
}
?>