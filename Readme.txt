
Penjelasan Aplikasi : Sistem Informasi Kantin TC

Sistem informasi kantin teknik informatika ITS yang berfungsi untuk membantu proses administrasi dan transaksi
fitur :
1. menambahkan dan menghapus barang
2. menambah dan menghapus stok
3. melihat informasi tentang stok (stok tipis, keseluruha, kadaluarsa)
4. menghapus barang kadaluarsa
5. buka dan tutup kantin
6. proses transaksi
7. laporan keuntungan perbulan, dan laporan administrasi lainnya


Tim Pengembang :
5113100172	Afif Ishamsyah H (Project Manager)
5113100077	Ilham Gurat A
5113100167	Rahmat Nazali S
5113100120	Muhammad Rizki Prawiraatmaja

Jobdesk :
5113100172	: Koding CI
5113100077	: koding CI
5113100167	: database
5113100120	: login & keamanan & finishing touch

Requirement :
- xampp
- sql client (kami menggunakan Sqlyog)


Step instalasi :
1. import database dari sqldump (KantinTCdump.sql)
	- buka sqlyog
	- klik kanan pada koneksi yang sudah ada
	- lalu pilih import / execute sql script
	- pilih file dump tersebut
	- execute

2. menaruh file web agar bisa diakses browser secara localhost (offline)
	- pindahkan folder Kantin yang ada didalam folder Kantin ke C://xampp/htdocs

3. membuka web
	- jalankan xampp, klik start pada apache dan mysql
	- untuk membuka homepage, ketikkan alamat berikut pada browser : localhost/Kantin/index.php/Welcome/kongou
		o user name : adminkantin
		o password : ibukantin
